#Homework: Homework 1.1

Install MongoDB on your computer and run it on the standard port.

Download the hw1-1.zip from Download Handout link and uncompress the file.

Change directory into hw1

Use mongorestore to restore the dump into your running mongod. Do this by opening a terminal 
window (mac) or cmd window (windows) and navigating to the directory so that the dump 
directory is directly beneath you. Now type:

```
mongorestore dump
```

Note you will need to have your path setup correctly to find mongorestore.

Now, using the Mongo shell, perform a `findOne` on the collection called `hw1_1` in the 
database `m101`. That will return one document. Please provide the value corresponding 
to the "answer" key (without the surrounding quotes) from the document returned.

##Solution
```
C:\week 01\homework_1_1\hw1-1\hw1-1>mongorestore dump
2015-06-01T14:59:59.934+0200    building a list of dbs and collections to restore from dump dir
2015-06-01T14:59:59.936+0200    reading metadata file from dump\m101\hw1_1.metadata.json
2015-06-01T14:59:59.937+0200    restoring m101.hw1_1 from file dump\m101\hw1_1.bson
2015-06-01T15:00:00.219+0200    restoring indexes for collection m101.hw1_1 from metadata
2015-06-01T15:00:00.220+0200    finished restoring m101.hw1_1
2015-06-01T15:00:00.220+0200    done
C:\week 01\homework_1_1\hw1-1\hw1-1>mongo
2015-06-01T15:00:38.340+0200 I CONTROL  Hotfix KB2731284 or later update is not installed, will zero-out data files
MongoDB shell version: 3.0.3
connecting to: test
> use m101
switched to db m101
> db.hw1_1.findOne()
{
        "_id" : ObjectId("51e4524ef3651c651a42331c"),
        "answer" : "Hello from MongoDB!"
}
>
```