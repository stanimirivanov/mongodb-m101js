#Homework: Homework 1.2

Your assignment for this part of the homework is to install the mongodb driver 
for Node.js and run the test application.To do this, first download the 
hw1-2.zip from Download Handout link, uncompress and change into the `hw1-2` directory:

```
cd hw1-2
```

Use mongorestore to restore the dump into your running mongod. Do this by opening a 
terminal window (mac) or cmd window (windows) and navigating to the directory so 
that the dump directory is directly beneath you. Now type:

```
mongorestore dump
```

Note you will need to have your path setup correctly to find mongorestore.

Then install the mongodb driver:

```
npm install mongodb
```

This should create a "node_modules" directory. Now run the application to get the answer 
to hw1-2:

```
node app.js
```

If you have the mongodb driver installed correctly, this will print out the message 
'Answer: ' followed by some additional text. Write that text in the text box below.

##Solution
```
C:\week 01\homework_1_2\hw1-2\hw1-2>mongorestore dump
2015-06-01T15:14:38.209+0200    building a list of dbs and collections to restore from dump dir
2015-06-01T15:14:38.212+0200    reading metadata file from dump\m101\hw1_2.metadata.json
2015-06-01T15:14:38.214+0200    restoring m101.hw1_2 from file dump\m101\hw1_2.bson
2015-06-01T15:14:38.218+0200    restoring indexes for collection m101.hw1_2 from metadata
2015-06-01T15:14:38.219+0200    finished restoring m101.hw1_2
2015-06-01T15:14:38.219+0200    done

C:\week 01\homework_1_2\hw1-2\hw1-2>npm install mongodb
mongodb@2.0.33 node_modules\mongodb
├── readable-stream@1.0.31 (string_decoder@0.10.31, inherits@2.0.1, isarray@0.0.1, core-util-is@1.0.1)
└── mongodb-core@1.1.32 (kerberos@0.0.12, bson@0.3.2)

C:\week 01\homework_1_2\hw1-2\hw1-2>node app.js
Answer: I like kittens
```