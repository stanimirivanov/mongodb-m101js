#Homework: Homework 1.3

Your assignment for this part of the homework is to install the mongodb driver for 
Node.js, Express, and other required dependencies and run the test application. 
This homework is meant to give you practice using the "package.json" file, which will 
include some of the code that we provide.

To do this, first download the hw1-3.zip from Download Handout link, uncompress and change 
into the hw1-3 directory:

```
cd hw1-3
```

Use mongorestore to restore the dump into your running mongod. Do this by opening a 
terminal window (mac) or cmd window (windows) and navigating to the directory so that 
the dump directory is directly beneath you. Now type:

```
mongorestore dump
```

Note you will need to have your path setup correctly to find mongorestore.

Then install all the dependencies listed in the 'package.json' file. Calling 'npm install' 
with no specific package tells npm to look for 'package.json':

```
npm install
```

This should create a "node_modules" directory with all the dependencies. Now run the 
application to get the answer to hw1-3:

```
node app.js
```

If you have all the dependencies installed correctly, this will print the message 
'Express server started on port 8080'. Navigate to 'localhost:8080' in a browser and 
write the text that is displayed on that page in the text box below.

##Solution
```
C:\week 01\homework_1_3\hw1-3\hw1-3>mongorestore dump
2015-06-01T15:31:31.574+0200    building a list of dbs and collections to restore from dump dir
2015-06-01T15:31:31.576+0200    reading metadata file from dump\m101\hw1_3.metadata.json
2015-06-01T15:31:31.578+0200    restoring m101.hw1_3 from file dump\m101\hw1_3.bson
2015-06-01T15:31:31.579+0200    restoring indexes for collection m101.hw1_3 from metadata
2015-06-01T15:31:31.580+0200    finished restoring m101.hw1_3
2015-06-01T15:31:31.580+0200    done

C:\week 01\homework_1_3\hw1-3\hw1-3>mongorestore dump
consolidate@0.9.1 node_modules\consolidate

crypto@0.0.3 node_modules\crypto

swig@0.14.0 node_modules\swig
└── underscore@1.8.3

express@3.20.3 node_modules\express
├── basic-auth@1.0.0
├── methods@1.1.1
├── range-parser@1.0.2
├── vary@1.0.0
├── merge-descriptors@1.0.0
├── content-type@1.0.1
├── parseurl@1.3.0
├── utils-merge@1.0.0
├── cookie-signature@1.0.6
├── cookie@0.1.2
├── escape-html@1.0.1
├── fresh@0.2.4
├── content-disposition@0.5.0
├── commander@2.6.0
├── depd@1.0.1
├── debug@2.2.0 (ms@0.7.1)
├── proxy-addr@1.0.8 (forwarded@0.1.0, ipaddr.js@1.0.1)
├── etag@1.5.1 (crc@3.2.1)
├── send@0.12.3 (destroy@1.0.3, ms@0.7.1, mime@1.3.4, on-finished@2.2.1, etag@1.6.0)
├── mkdirp@0.5.0 (minimist@0.0.8)
└── connect@2.29.2 (on-headers@1.0.0, pause@0.0.1, response-time@2.3.1, vhost@3.0.0, method-override@2.3.3, basic-auth-connect@1.0.0, bytes@1.0.0, serve-static@1.9.3, connect-timeo
ut@1.6.2, http-errors@1.3.1, cookie-parser@1.3.5, qs@2.4.2, finalhandler@0.3.6, morgan@1.5.3, serve-favicon@2.2.1, csurf@1.7.0, express-session@1.10.4, type-is@1.6.2, errorhandler@
1.3.6, compression@1.4.4, multiparty@3.3.2, body-parser@1.12.4, serve-index@1.6.4)

mongodb@1.3.23 node_modules\mongodb
├── kerberos@0.0.3
└── bson@0.2.5

C:\week 01\homework_1_3\hw1-3\hw1-3>node app.js
Failed to load c++ bson extension, using pure JS version
Express server started on port 8080

Hello, Agent 007.
```