#Homework: Homework 2.1

In this problem, you will be using an old weather dataset. 
Download weather_data.csv from the Download Handout link. 
This is a comma separated value file that you can import into MongoDB as follows:

```
mongoimport --type csv --headerline weather_data.csv -d weather -c data
```

You can verify that you've imported the data correctly by running the following 
commands in the mongo shell:

```
> use weather
> db.data.find().count()
> 2963
```

##Solution
```
C:\week 02\homework_2_1>mongoimport --type csv --headerline weather_data.csv -d weather -c data
2015-06-09T12:36:48.370+0200    connected to: localhost
2015-06-09T12:36:48.622+0200    imported 2963 documents
```

```
C:\week 02\homework_2_1>mongo
2015-06-09T12:38:06.778+0200 I CONTROL  Hotfix KB2731284 or later update is not installed, will zero-out data files
MongoDB shell version: 3.0.3
connecting to: test
>

> use weather
switched to db weather
> db.data.find().count()
2963
>

> db.data.find({"Wind Direction" : {$gte : 180, $lte : 360}}).sort({Temperature : 1}).limit(1)
{ "_id" : ObjectId("5576c1c05b9e8b87b42df575"), "Day" : 24, "Time" : 153, "State" : "New Mexico", "Airport" : "SAF", "Temperature" : 0, "Humidity" : 87, "Wind Speed" : 5, "Wind Direction" : 260, "Station Pressure" : 23.88, "Sea Level Pressure" : 274 }
>
```