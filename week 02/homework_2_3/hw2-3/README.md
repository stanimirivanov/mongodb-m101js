#Homework: Homework 2.3

You should see four files in the 'blog' directory: app.js, users.js, posts.js and 
sessions.js. There is also a 'views' directory which contains the templates for the 
project and a 'routes' directory which contains our express routes. 

If everything is working properly, you should be able to start the blog by typing:

```
npm install
node app.js
```

Note that this requires Node.js to be correctly installed on your computer.
After you run the blog, you should see the message: 
Express server listening on port 8082

If you goto http://localhost:8082 you should see the front page of the blog. Here are 
 URLs that must work when you are done. 

 ```
http://localhost:8082/signup
http://localhost:8082/login
http://localhost:8082/logout
```

When you login or sign-up, the blog will redirect to http://localhost:8082/welcome and that 
must work properly, welcoming the user by username. 

We have removed parts of the code that uses the Node.js driver to query MongoDB from 
users.js and marked the area where you need to work with "TODO: hw2.3". You should not 
need to touch any other code. The database calls that you are going to add will add a 
new user upon sign-up and validate a login by retrieving the right user document. 

The blog stores its data in the blog database in two collections, users and sessions. Here 
are two example docs for a username ‘sverch’ with password ‘salty’. You can insert these 
if you like, but you don’t need to.

```
> use blog
switched to db blog
> db.users.find()
{ "_id" : "sverch", "password" : "$2a$10$wl4bNB/5CqwWx4bB66PoQ.lmYvxUHigM1ehljyWQBupen3uCcldoW" }
> db.sessions.find()
{ "username" : "sverch", "_id" : "8d25917b27e4dc170d32491c6247aabba7598533" }
>
```

Once you have the the project working, the following steps should work: 

```
go to http://localhost:8082/signup
create a user
```

It should redirect you to the welcome page and say: welcome username, where username is the 
user you signed up with. Now:

``` 
    Goto http://localhost:8082/logout
    Now login http://localhost:8082/login
```

Ok, now it’s time to validate you got it all working.

##Solution
```
C:\week 02\homework_2_3\hw2-3\hw2-3\blog>npm install
...
swig@0.14.0 node_modules\swig
└── underscore@1.8.3

express@3.20.3 node_modules\express
├── basic-auth@1.0.0
├── methods@1.1.1
├── range-parser@1.0.2
├── content-type@1.0.1
├── merge-descriptors@1.0.0
├── cookie-signature@1.0.6
├── vary@1.0.0
├── utils-merge@1.0.0
├── parseurl@1.3.0
├── cookie@0.1.2
├── escape-html@1.0.1
├── fresh@0.2.4
├── content-disposition@0.5.0
├── commander@2.6.0
├── depd@1.0.1
├── debug@2.2.0 (ms@0.7.1)
├── proxy-addr@1.0.8 (forwarded@0.1.0, ipaddr.js@1.0.1)
├── mkdirp@0.5.0 (minimist@0.0.8)
├── etag@1.5.1 (crc@3.2.1)
├── send@0.12.3 (destroy@1.0.3, ms@0.7.1, mime@1.3.4, etag@1.6.0, on-finished@2.2.1)
└── connect@2.29.2 (vhost@3.0.0, response-time@2.3.1, on-headers@1.0.0, method-override@2.3.3, serve-static@1.9.3, pause@0.0.1, basic-auth-connect@1.0.0, bytes@1.0.0, cookie-parser
@1.3.5, connect-timeout@1.6.2, http-errors@1.3.1, qs@2.4.2, morgan@1.5.3, finalhandler@0.3.6, serve-favicon@2.2.1, csurf@1.7.0, express-session@1.10.4, type-is@1.6.3, multiparty@3.
3.2, compression@1.4.4, errorhandler@1.3.6, body-parser@1.12.4, serve-index@1.6.4)

mongodb@1.4.38 node_modules\mongodb
├── readable-stream@1.0.33 (string_decoder@0.10.31, inherits@2.0.1, isarray@0.0.1, core-util-is@1.0.1)
├── kerberos@0.0.11 (nan@1.8.4)
└── bson@0.2.21 (nan@1.7.0)

C:\week 02\homework_2_3\hw2-3\hw2-3\blog>
```
```
C:\week 02\homework_2_3\hw2-3\hw2-3\blog>node app.js
connect deprecated multipart: use parser (multiparty, busboy, formidable) npm module instead node_modules\express\node_modules\connect\lib\middleware\bodyParser.js:56:20
connect deprecated limit: Restrict request size at location of read node_modules\express\node_modules\connect\lib\middleware\multipart.js:86:15
Express server listening on port 8082
```